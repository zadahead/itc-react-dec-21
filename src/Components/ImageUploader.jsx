import axios from "axios";
import { useState } from "react";

const ImageUploader = () => {
    const [img, setImg] = useState(null);
    const [opac, setOpac] = useState(10);

    const [file, setFile] = useState();

    const handleImageSelect = (e) => {
        const f = e.target.files[0];
        const blob = URL.createObjectURL(f);

        console.log(blob);
        setImg(blob);
        setFile(f);
        setOpac(10);
    }

    const handleUpload = async () => {

        const formData = new FormData();
        formData.append('image', file);

        const config = {
            onUploadProgress: (progress) => {
                const completed = Math.round((progress.loaded * 100) / progress.total);
                console.log('progress: ', completed);
                setOpac(completed);
            }
        }

        const resp = await axios.post('http://localhost:5000/upload', formData, config);
        console.log(resp);
    }

    return (
        <div>
            <h2>Image Uploader</h2>
            <input type="file" onChange={handleImageSelect} accept="image/png, image/jpeg" />
            {img && <img src={img} alt="upload" style={{ width: '200px', opacity: opac / 100 }} />}

            <button onClick={handleUpload}>Upload</button>
        </div >
    )
}

export default ImageUploader;