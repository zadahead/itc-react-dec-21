import { useContext } from 'react';
import { CountContext } from 'Context/countContext';

const CounterDisplay = () => {
    const { count } = useContext(CountContext);
    return (
        <div>
            <h4>{count}</h4>
        </div>
    )
}

export default CounterDisplay;