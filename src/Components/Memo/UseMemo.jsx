import { useState, useMemo } from 'react';
import { Btn, Line } from 'UIKit';

const UseMemory = () => {
    //VVVV
    const [isRed, setIsRed] = useState(false);
    const [count, setCount] = useState(0);

    const handleCount = () => {
        setCount(count + 1);
    }

    const handleToggle = () => {
        setIsRed(!isRed);
    }

    const expendCount = (curr) => {
        for (let i = 0; i < 1000000000; i++) {
            curr++;
        }
        return curr;
    }

    const expendCountResult = useMemo(() => {
        return expendCount(count);
    }, [count])


    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h1 style={styleCss}>Memo, {expendCountResult}</h1>
            <Line>
                <Btn onClick={handleCount}>Add</Btn>
                <Btn onClick={handleToggle}>Toggle</Btn>
            </Line>
        </div>
    )
}

export default UseMemory;