const Wrapper = (props) => {
    console.log('props', props);


    return (
        <div>
            <h1>This is a wrapper!</h1>
            <div>
                {props.header}
            </div>
            <div>
                {props.children}
            </div>
            <div>
                footer
            </div>
        </div>
    )
}

export default Wrapper;