import { CountContext } from 'Context/countContext';
import { useContext } from 'react';
import { Btn } from 'UIKit';

const CounterAdd = () => {
    const context = useContext(CountContext);

    return (
        <div>
            <Btn onClick={context.handleCount}>Add Count</Btn>
        </div>
    )
}

export default CounterAdd;