import { useState, useEffect } from "react";

/*
1) mounted
2) updated
3) unmounted
*/

const Counter = () => {
    console.log('render');

    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(true);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleToggle = () => {
        setIsRed(!isRed);
    }

    //componentDidMount
    useEffect(() => {
        console.log('componentDidMount!!!!!');

        //componentWillUnmount
        return () => {
            console.log('componentWillUnmount!!!!!!!');
        }
    }, [])

    //componentDidUpdate
    useEffect(() => {
        console.log('componentDidUpdate');

        //componentWillUnmount
        return () => {
            console.log('componentWillUnmount - < update ');
        }
    })


    //componentDidUpdate - count
    useEffect(() => {
        console.log('componentDidUpdate', count);

        //componentWillUnmount - count
        return () => {
            console.log('componentWillUnmount - < update ', count);
        }
    }, [count])


    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }


    return (
        <div>
            <h1 style={styleCss}>Count, {count}</h1>
            <button onClick={handleAdd}>Add</button>
            <button onClick={handleToggle}>Toggle</button>
        </div>
    )
}

export default Counter;