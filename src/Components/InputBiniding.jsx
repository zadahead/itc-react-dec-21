import { useState, useRef } from "react";
import { Btn, Input } from 'UIKit';


const InputBinding = () => {

    const [value, setValue] = useState('');
    const inputRef = useRef();

    const handleOnChange = (e) => {
        setValue(e.target.value);
    }

    const logValue = () => {
        console.log(value);
    }

    const logUnControlledValue = () => {
        console.log(inputRef.current.value);
    }

    return (
        <div>
            <div>
                <h1>Controlled Input</h1>
                <Input value={value} onChange={handleOnChange} />
            </div>
            <div>
                <h1>Uncontrolled Input</h1>
                <input ref={inputRef} />
            </div>
            <div>
                <Btn onClick={logValue}>Log Input</Btn>
                <Btn onClick={logUnControlledValue}>Log Uncontrolled Input</Btn>
            </div>
        </div>
    )
}

export default InputBinding;