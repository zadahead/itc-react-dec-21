import ListItem from "./ListItem"

const List = (props) => {
    return props.items.map((i, index) => {
        return <ListItem key={i} item={i} onDelete={() => props.onDelete(index)} />
    })
}

export default List;