import Btn from "../../UIKit/Elements/Btn/Btn"

const AddNew = (props) => {
    return (
        <div>
            <Btn onClick={props.onAdd}>Add</Btn>
        </div>
    )
}

export default AddNew;