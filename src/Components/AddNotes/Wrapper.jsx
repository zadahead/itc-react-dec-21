import { useState } from "react";
import AddNew from "./AddNew";
import List from "./List";

const Wrapper = () => {
    const [list, setList] = useState([1, 2, 3, 4, 5])

    const handleAdd = () => {
        const newList = [...list, list.length + 1];
        setList(newList);
    }

    const handleRemove = (index) => {
        const newList = [...list];
        newList.splice(index, 1);
        setList(newList);
    }

    return (
        <div>
            <AddNew onAdd={handleAdd} />
            <List items={list} onDelete={handleRemove} />
        </div>
    )
}


export default Wrapper;