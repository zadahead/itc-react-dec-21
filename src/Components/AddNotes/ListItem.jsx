import Line from "../../UIKit/Layouts/Line/Line";

const ListItem = (props) => {
    return (
        <Line>
            <h2>Item {props.item}</h2>
            <button onClick={props.onDelete}>remove</button>
        </Line>
    )
}

export default ListItem;