import Trigger from './Trigger';

export const Welcome = () => {
    const value = 'Great Value';

    const welcomeAlert = (value) => {
        console.log(`dddd`, value);
    }

    return (
        <div>
            <h1>Welcome!</h1>
            <Trigger onClick={welcomeAlert} value={value} />
        </div>
    )
}

export const TinyWelcome = () => {
    return (
        <h2>Tiny Welcome</h2>
    )
}


export const add = (a, b) => {
    return a + b;
}

export default Welcome;