import React, { useState } from "react";
import CounterFunc from "./CounterFunc";

const Parent = () => {
    //state
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <div>
            <CounterFunc count={count} handleAdd={handleAdd} />
            <CounterFunc count={count} handleAdd={handleAdd} />
        </div>
    )
}

export default Parent;