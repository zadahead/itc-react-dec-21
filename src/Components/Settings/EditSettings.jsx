import { Settings } from "./Settings";

const existingUser = {
    name: 'mosh',
    lastName: 'something',
    age: '25',
    phone: '0544646464',
    location: 'home'
}

const EditSettings = () => {
    const handleEdit = (values) => {
        console.log('Edit', values);
    }
    return (
        <Settings
            headline="Edit Settings"
            btnTitle="Update"
            onSubmit={handleEdit}
            user={existingUser}
        />
    )
}

export default EditSettings;