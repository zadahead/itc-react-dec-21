import { useInputsForm } from "Hooks/useInputsForm";
import { Btn, Input } from "UIKit";

const emptyUser = {
    name: '',
    lastName: '',
    age: '',
    phone: '',
    location: ''
}

export const Settings = ({ headline, btnTitle, onSubmit, user = emptyUser }) => {

    //logic
    const [inputs, getValues] = useInputsForm(user);

    //save
    const handleSave = () => {
        onSubmit(getValues())
    }

    //render
    return (
        <div>
            <div>
                <h1>{headline}</h1>
            </div>
            {
                inputs.map((i, index) => (
                    <div key={index}>
                        <Input {...i} />
                    </div>
                ))
            }
            <div>
                {<Btn onClick={handleSave}>{btnTitle}</Btn>}
            </div>
        </div>
    )
}