import { Settings } from "./Settings";

const NewSettings = () => {
    const handleSave = (values) => {
        console.log('Save', values);
    }
    return (
        <Settings
            headline="New Settings"
            btnTitle="Save"
            onSubmit={handleSave}
        />
    )
}

export default NewSettings;