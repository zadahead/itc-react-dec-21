
import React from 'react';

class CounterSwitcherClass extends React.Component {
    state = {
        count: 0,
        isRed: false
    }

    handleAddCount = () => {
        console.log('handleAddCount clicked');
        this.setState({
            count: this.state.count + 1
        })
    }

    toggle = () => {
        this.setState({
            isRed: !this.state.isRed
        })
    }

    combine = () => {
        this.handleAddCount();
        this.toggle();
    }

    render = () => {
        console.log('render', this.state);

        const styleCss = {
            color: this.state.isRed ? 'red' : 'blue'
        }

        return (
            <div>
                <h1 style={styleCss}>Count, {this.state.count}</h1>
                <button onClick={this.handleAddCount}>Add</button>
                <button onClick={this.toggle}>Toggle</button>
                <button onClick={this.combine}>Combine</button>
            </div>
        )
    }
}

export default CounterSwitcherClass;