const Trigger = (props) => {


    const alertMe = () => {
        console.log('Hello!');

        props.onClick(props.value);
    }

    return (
        <div>
            <h1 onClick={alertMe}>Click ME!</h1>
        </div>
    )
}

export default Trigger;