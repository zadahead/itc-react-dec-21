
import React from 'react';

class ColorSwitch extends React.Component {

    state = {
        isRed: false
    }

    toggle = () => {
        this.setState({
            isRed: !this.state.isRed
        })
    }

    render = () => {

        const styleCss = {
            color: this.state.isRed ? 'red' : 'blue'
        }

        return (
            <div>
                <h1 style={styleCss}>Toggler</h1>
                <button onClick={this.toggle}>Toggle</button>
            </div>
        )
    }
}

export default ColorSwitch;