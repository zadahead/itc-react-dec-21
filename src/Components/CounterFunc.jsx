import { Context } from "Context/stringContext";
import { useColorSwitch } from "Hooks/useColorSwitch";
import { useCounter } from "Hooks/useCounter";
import { useContext } from "react";
import Line from "UIKit/Layouts/Line/Line";

const CounterFunc = (props) => {
    console.log('render');

    return (
        <div>
            <h5>Count, {props.count}</h5>
            <button onClick={props.handleAdd}>Add</button>
        </div>
    )
}





export const Counter = () => {
    const user = useContext(Context);

    //logic
    const { count, handleAdd } = useCounter(10);
    const { styleCss, handleSwitch } = useColorSwitch();

    //render
    return (
        <Line>
            <h3 style={styleCss}>Count, {count} -- {user.name}</h3>
            <button onClick={handleAdd}>Add</button>
            <button onClick={handleSwitch}>Toggle</button>
        </Line>
    )
}




export default CounterFunc;