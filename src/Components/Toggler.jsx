import { useState } from "react";
import Counter from "./Counter";

const Toggler = () => {
    const [isDisplay, setIsDisplay] = useState(true);

    const handleDisplay = () => {
        setIsDisplay(!isDisplay);
    }


    return (
        <div>
            <h1>Toggler</h1>
            <button onClick={handleDisplay}>Toggle</button>

            <div>
                {isDisplay && <Counter />}
            </div>
        </div>
    )
}

export default Toggler;