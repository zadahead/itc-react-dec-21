
import React from 'react';

class CounterClass extends React.Component {
    state = {
        count: 0
    }

    handleAddCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    componentDidMount = () => {
        console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }

    render = () => {
        console.log('render');

        return (
            <div>
                <h1>Count, {this.state.count}</h1>
                <button onClick={this.handleAddCount}>Add</button>
            </div>
        )
    }
}

export default CounterClass;