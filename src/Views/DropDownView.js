import { useState, useContext } from "react";
import { Box, Center, Dropdown } from "UIKit";
import { Context } from 'Context/stringContext';


const list = [
    { id: 1, value: 'item 1' },
    { id: 2, value: 'item 2' },
    { id: 3, value: 'item 3333' },
    { id: 4, value: 'item 4' }
]

const DropDownView = () => {
    const [selected, setSelected] = useState(null);

    const handleSelect = (value) => {
        setSelected(value);
    }

    return (
        <Center>
            <Box>
                <div>
                    <h1>Dropdownview</h1>
                    <Dropdown list={list} selectedId={selected} onChange={handleSelect} />
                </div>
            </Box>
        </Center>
    )
}

export default DropDownView;