import EditSettings from "Components/Settings/EditSettings";
import NewSettings from "Components/Settings/NewSettings";


const SettingsWrapper = () => {
    return (
        <div>
            <NewSettings />
            <EditSettings />
        </div>
    )
}

export default SettingsWrapper;