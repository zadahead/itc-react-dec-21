import axios from "axios";
import { useEffect, useState } from "react";
import ImageContainer from "../UIKit/Layouts/ImageContainer/ImageContainer";

const url = 'https://api.flickr.com/services/rest/';

/*
 method=flickr.photos.search 
 api_key=636e1481b4f3c446d26b8eb6ebfe7127
 tags=food
 per_page=24
 format=json
 nojsoncallback=1
*/

const params = {
    'method': 'flickr.photos.search',
    'api_key': '636e1481b4f3c446d26b8eb6ebfe7127',
    'tags': 'birds',
    'per_page': '24',
    'format': 'json',
    'nojsoncallback': '1'
}



const Photos = () => {
    const [list, setList] = useState([]);
    const [tag, setTag] = useState('food')

    useEffect(() => {
        fetchData();
    }, [])

    const fetchData = async () => {
        const diconstruct = { ...params, tags: tag };
        const resp = await axios.get(url, { params: diconstruct });

        console.log(resp.data.photos.photo);
        setList(resp.data.photos.photo);
    }

    const renderList = () => {
        return (
            <ImageContainer>
                {
                    list.map(img => {
                        const src = `https://farm66.staticflickr.com/${img.server}/${img.id}_${img.secret}_m.jpg`;
                        return (
                            <div key={img.id}>
                                <img src={src} alt={img.title} />
                            </div>
                        )
                    })
                }
            </ImageContainer>
        )
    }

    return (
        <div>
            <h1>Photos!</h1>
            {
                list.length ? renderList() : <h1>loading....</h1>
            }
        </div>
    )
}

export default Photos;