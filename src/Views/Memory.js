
import { useState, memo, useCallback, useRef, forwardRef, useEffect } from 'react';
import { Btn, Line } from 'UIKit';
import UseMemo from 'Components/Memo/UseMemo';

const WelcomeFunc = (props) => {
    console.log('<Welcome /> render');

    return (
        <div>
            <Line>
                <h2>Welcome {props.name}</h2>
                <Btn onClick={props.onHello}>Say hello</Btn>
            </Line>
        </div>
    );
}

const Welcome = memo(WelcomeFunc);

const Memory = () => {
    const [count, setCount] = useState(0);
    const name = "mosh";

    const someRef = useRef();


    const handleCount = () => {
        setCount(count + 1);
    }

    const sayHello = useCallback(() => {
        console.log('HEY!');
    }, [])



    return (
        <div>
            <h1>UseCallback, {count}</h1>
            <Wrapper ref={someRef} />
            <Welcome name={name} onHello={sayHello} />
            <Btn onClick={handleCount}>Add</Btn>
        </div>
    )
}


const Wrapper = forwardRef((props, ref) => {
    return (
        <div>
            <h1>Hello</h1>
            <h2 ref={ref}>Hllo H2</h2>
            <h3 >Hllo H3</h3>
        </div>
    )
})

export default Memory;