import axios from "axios";
import { useInputs } from "Hooks/useInputs";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Btn, Center, Input, Rows } from "UIKit";
import { myFetch } from 'Services/myFetch';


const RegisterView = () => {
    const [name, age, email, password, repassword] = useInputs(5);
    const [error, setError] = useState('');
    const navigate = useNavigate();

    const handleRegister = async () => {
        try {
            await myFetch('http://localhost:4001/auth/register', {
                name: name.value,
                age: parseInt(age.value),
                email: email.value,
                password: password.value,
                repassword: repassword.value
            })

            navigate('/login');
        } catch (error) {
            setError(error);
        }
    }
    return (
        <div>
            <Center>
                <Rows>
                    <h2>Register View</h2>
                    <Input {...name} placeholder="Your Name..." />
                    <Input {...age} placeholder="Your Age..." />
                    <Input {...email} placeholder="Your Email..." />
                    <Input {...password} placeholder="Your Password..." />
                    <Input {...repassword} placeholder="Repeat Password..." />
                    <Btn onClick={handleRegister}>Register</Btn>
                    {error && <h4 style={{ color: 'red' }}>{error}</h4>}
                </Rows>
            </Center>
        </div>
    )
}

export default RegisterView;