import { authContext } from 'Auth/authContext';
import { useInputs } from 'Hooks/useInputs';
import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { myFetch } from 'Services/myFetch';
import { Btn, Center, Input, Rows } from 'UIKit';



const LoginView = () => {
    const [email, password] = useInputs(2);
    const [error, setError] = useState('');
    const auth = useContext(authContext);
    const navigate = useNavigate();
    console.log(auth);

    const handleLogin = async () => {
        try {
            const data = await myFetch('http://localhost:4001/auth/login', {
                email: email.value,
                password: password.value,
            })
            setError('');
            console.log(data);
            auth.login(data);
            navigate('/myprofile');
        } catch (error) {
            setError(error);
        }
    }

    return (
        <div>
            <Center>
                <Rows>
                    <h1>Login View</h1>
                    <Input {...email} placeholder="Email.." />
                    <Input {...password} placeholder="Password.." />
                    <Btn onClick={handleLogin}>Login</Btn>

                    {error && <h4 style={{ color: 'red' }}>{error}</h4>}
                </Rows>
            </Center>
        </div>
    )
}

export default React.memo(LoginView);