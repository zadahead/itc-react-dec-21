import { authContext } from "Auth/authContext";
import { useContext } from "react";
import { Btn } from "UIKit";

const NavUser = () => {
    const { logout } = useContext(authContext);

    return (
        <div>
            <h4>User Info</h4>
            <Btn onClick={logout}>Logout</Btn>
        </div>
    )
}

export default NavUser;