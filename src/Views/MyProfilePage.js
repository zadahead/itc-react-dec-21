
import { authContext } from "Auth/authContext"
import axios from "axios"
import ImageUploader from "Components/ImageUploader"
import { useContext, useEffect } from "react"
import { Center } from "UIKit"

export const MyProfilePage = () => {

    const { logout } = useContext(authContext);

    useEffect(() => {
        getMe();
    }, [])

    const getMe = async () => {
        try {
            const resp = await axios.get('http://localhost:4001/users/me', {
                headers: {
                    Authorization: localStorage.getItem('access_token')
                }
            });
            console.log(resp);
        } catch (error) {
            console.log('ERROR', error);
            logout();
        }
    }

    const handleAdmin = async () => {
        try {
            const resp = await axios.get('http://localhost:4001/users/admin', {
                headers: {
                    Authorization: localStorage.getItem('access_token')
                }
            });
            console.log(resp);
        } catch (error) {
            console.log('ERROR', error);
            logout();
        }
    }

    return (
        <div>
            <Center>
                <h2>My Profile</h2>
                <button onClick={handleAdmin}>am I an Admin</button>
                <ImageUploader />
            </Center>
        </div>
    )
}