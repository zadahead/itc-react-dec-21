import axios from 'axios';


export const myFetch = (url, data) => {
    return new Promise(async (res, rej) => {
        try {
            const reps = await axios.post(url, data, { withCredentials: true });
            res(reps.data);
        } catch (error) {
            const message = error?.response?.data?.message;
            if (Array.isArray(message)) {
                rej(message.map(i => i.message).join(', '));
            } else {
                rej(message || 'Error Accured');
            }
        }
    })
}