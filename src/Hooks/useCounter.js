import { useState, useEffect } from "react";

export const useCounter = (initialValue = 0) => {
    //logic
    const [count, setCount] = useState(initialValue);

    const handleAdd = () => {
        setCount(count + 1);
    }

    useEffect(() => {
        console.log('componentDidMount');
    }, [])

    useEffect(() => {
        console.log('count did update');
    }, [count])

    //what does it NEEDS to return
    return {
        count,
        handleAdd
    }
}