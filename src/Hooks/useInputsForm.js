import { useInputs } from './useInputs';

export const useInputsForm = (form) => {
    const values = Object.values(form);
    const inputs = useInputs(values.length, values);

    const getValues = () => {
        const keys = Object.keys(form);
        return keys.reduce((state, key, index) => {
            state[key] = inputs[index].value;
            return state;
        }, {})
    }
    return [inputs, getValues];
}