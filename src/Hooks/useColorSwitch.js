import { useState } from 'react';

export const useColorSwitch = () => {
    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        color: isRed ? 'red' : 'green'
    }

    return {
        styleCss,
        handleSwitch
    }
}