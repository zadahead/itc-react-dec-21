import { useState } from 'react';

export const useInputs = (count, values = []) => {
    const handleChange = (index, value) => {
        const inputClone = [...inputs];
        inputClone[index].value = value;
        setInputs(inputClone);
    }

    const arr = new Array(count).fill(0);

    const reduce = arr.map((i, index) => {
        return {
            value: values[index] || '',
            onChange: (e) => handleChange(index, e.target.value)
        }
    }, []);

    const [inputs, setInputs] = useState(reduce);


    return inputs;
}