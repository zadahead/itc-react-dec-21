import { useState } from 'react';
import { Grid, Line, Inner, Alert } from 'UIKit';

import 'App.css';
import { Counter } from 'Components/CounterFunc';

import { NavLink, Routes, Route } from 'react-router-dom';

import { CountContext } from 'Context/countContext';

import RegisterView from 'Views/RegisterView';
import LoginView from 'Views/LoginView';
import { AuthProvider } from 'Auth/AuthProvider';
import { MyProfilePage } from 'Views/MyProfilePage';
import AuthRoute from 'Auth/AuthRoute';
import NavUser from 'Views/NavUser';


const App = () => {
    const [count, setCount] = useState(0);
    const [isAlert, setIsAlert] = useState(false);


    const handleCount = () => {
        setCount(count + 1);
    }

    const closeAlert = () => {
        setIsAlert(false);
    }

    return (
        <AuthProvider>
            <CountContext.Provider value={{ count, handleCount }}>
                <div className='App'>
                    {isAlert && <Alert message="I Have been transported!" onClose={closeAlert} />}

                    <Grid>
                        <div>
                            <Inner>
                                <Line between>
                                    <h3>logo</h3>
                                    <Line>
                                        <NavLink to="/myprofile">My Profile</NavLink>
                                        <NavLink to="/register">Register</NavLink>
                                        <NavLink to="/login">Login</NavLink>
                                    </Line>
                                    <div>
                                        <NavUser />
                                    </div>
                                </Line>
                            </Inner>
                        </div>
                        <div>
                            <Inner>
                                <Routes>
                                    <Route path="/register" element={<RegisterView />} />
                                    <Route path="/login" element={<LoginView />} />
                                    {/* <Route path="/myprofile" element={<MyProfilePage />} /> */}
                                    <Route path='/myprofile' element={
                                        <AuthRoute>
                                            <MyProfilePage />
                                        </AuthRoute>
                                    }
                                    />
                                </Routes>
                            </Inner>
                        </div>
                        <div>Footer</div>
                    </Grid>
                </div>
            </CountContext.Provider>
        </AuthProvider>
    )
}

export default App;



/*

1) loads (rendered) - Mounted
2) updated (re-rendered) - Updated
3) un-loads (removed) - UnMounted

did / will

component did Mount
component will Mount 

*/