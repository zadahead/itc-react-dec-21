import reactDom from "react-dom";
import Btn from "../Btn/Btn";

import './Alert.css';

export const Modal = (props) => {
    return (
        reactDom.createPortal(
            props.children,
            document.getElementById('modal')
        )
    )
}

export const Alert = (props) => {
    return (
        <Modal>
            <div className="Alert">
                <div className="message">
                    <div>
                        <h1>{props.message}</h1>
                    </div>
                    <div>
                        <Btn onClick={props.onClose}>Close</Btn>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default Modal;