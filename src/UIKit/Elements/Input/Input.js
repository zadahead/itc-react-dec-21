
import './Input.css';
export const Input = (props) => {

    return (
        <div className="Input">
            <input {...props} />
        </div>
    )
}

export default Input;