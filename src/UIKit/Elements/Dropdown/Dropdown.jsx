import { useState, useEffect, useRef } from 'react';
import Line from 'UIKit/Layouts/Line/Line';
import Icon from '../Icon/Icon';
import './Dropdown.css';




export const Dropdown = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const wrapperRef = useRef();

    useEffect(() => {
        document.body.addEventListener('click', handleBodyClick);

        return () => {
            // trigger once, on un load. 
            document.body.removeEventListener('click', handleBodyClick);
        }
    }, []) // [] trigger once, on load. 

    const handleIsOpen = () => {
        setIsOpen(!isOpen);
    }

    const handleSelect = (item) => {
        props.onChange(item.id);
        setIsOpen(false);
    }

    const handleBodyClick = (e) => {
        console.log('handleBodyClick');
        if (!wrapperRef.current) { return }

        if (!wrapperRef.current.contains(e.target)) {
            setIsOpen(false);
        }
    }

    const getSelected = () => {
        const selected = props.list.find(i => i.id === props.selectedId);
        if (selected) {
            return selected.value;
        }
        return 'Please Select';
    }

    return (
        <div className='Dropdown' ref={wrapperRef}>
            <div className='trigger' onClick={handleIsOpen}>
                <Line between>
                    <h3>{getSelected()}</h3>
                    <Icon i="chevron-down" />
                </Line>
            </div>
            {
                isOpen &&
                <div className='list'>
                    {
                        props.list.map(i => (
                            <div
                                key={i.id}
                                onClick={() => handleSelect(i)}
                                className={`${i.id === props.selectedId ? 'selected' : ''}`}
                            >
                                {i.value}
                            </div>
                        ))
                    }
                </div>
            }
        </div>
    )
}