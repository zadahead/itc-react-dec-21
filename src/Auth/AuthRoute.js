import { useContext } from "react";
import { authContext } from "./authContext";
import { Navigate } from "react-router-dom";

const AuthRoute = ({ children }) => {
    const { isLoggedIn, isLoading } = useContext(authContext);

    if (isLoading) {
        return <></>
    }

    if (isLoggedIn) {
        return children;
    }

    return <Navigate to="/login" />

}

export default AuthRoute;