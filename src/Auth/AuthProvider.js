import { useEffect, useState } from "react"
import { authContext } from "./authContext"

export const AuthProvider = ({ children }) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    //const [user, setUser] = useState({});

    useEffect(() => {
        const access_token = localStorage.getItem('access_token');
        if (access_token) {
            setIsLoggedIn(true);
        }

        setIsLoading(false);
        // const user_details = localStorage.getItem('user_details');
        // if (user_details) {
        //     setUser(JSON.parse(user_details));
        // }
    }, [])

    const login = ({ user, access_token, refresh_token }) => {
        //setUser(user);
        //access_token
        //refresh_token
        //localStorage.setItem('user_details', JSON.stringify(user));
        localStorage.setItem('access_token', access_token);
        //localStorage.setItem('refresh_token', refresh_token);
        setIsLoggedIn(true);
    }

    const logout = () => {
        localStorage.removeItem('access_token');
        setIsLoggedIn(false);
    }

    return (
        <authContext.Provider value={{ isLoading, isLoggedIn, login, logout }}>
            {children}
        </authContext.Provider>
    )
}